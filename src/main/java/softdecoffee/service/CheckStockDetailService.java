/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.List;
import softdecoffee.dao.CheckStockDao;
import softdecoffee.dao.CheckStockDetailDao;
import softdecoffee.model.CheckStock;
import softdecoffee.model.CheckStockDetail;

/**
 *
 * @author Lenovo
 */
public class CheckStockDetailService {
    public CheckStockDetail getById(int id){
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();       
        return checkStockDetailDao.get(id);
    }
    
    public List<CheckStockDetail>getByCheckID(int id){
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        return checkStockDetailDao.getByCheckId(id);
        
    }
    
    public List<CheckStockDetail> getCheckStocks(){
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        return checkStockDetailDao.getAll(" SCD_CODE asc");
    }
    
    
    public CheckStockDetail addNew(CheckStockDetail editedReceiptDetail) {
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        return checkStockDetailDao.save(editedReceiptDetail);
    }

    public CheckStockDetail update(CheckStockDetail editedReceiptDetail) {
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        return checkStockDetailDao.update(editedReceiptDetail);
    }

    public int delete(CheckStockDetail editedReceiptDetail) {
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        return checkStockDetailDao.delete(editedReceiptDetail);
    }

}
