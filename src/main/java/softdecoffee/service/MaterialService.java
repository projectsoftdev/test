/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;


import java.util.ArrayList;
import java.util.List;
import softdecoffee.dao.MaterialDao;
import softdecoffee.model.Material;

/**
 *
 * @author Nobpharat
 */
public class MaterialService {   
    public Material getById(int id){
        MaterialDao materialDao = new MaterialDao();
        return materialDao.get(id);
    }
    public List<Material> getMaterials(){
        MaterialDao materialDao = new MaterialDao();    
        return materialDao.getAll(" M_CODE asc");
    }
    public ArrayList<Material> getMaterialsOrderById(){
        MaterialDao materialDao = new MaterialDao();
        return (ArrayList<Material>) materialDao.getAll(" M_CODE asc");
    }

    public Material addNew(Material editedMaterial) {
       MaterialDao materialDao = new MaterialDao();
        return materialDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.update(editedMaterial);
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }
}

