/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import softdecoffee.dao.MaterialDao;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.service.MaterialService;

/**
 *
 * @author Lenovo
 */
public class CheckStockDetail {
    private static int nextId = 1;
    private int Id;
    private int MaterialId;
    private int CheckStockId;
    private double remainingAmount;
    private int expAmount;
    private int totalLost;
//    private MaterialDao materialDao = new MaterialDao();
//    private Material material;
//       

            
    public CheckStockDetail(int Id, int MaterialId, int CheckStockId, double remainingAmount, int expAmount, int totalLost) {
        this.Id = Id;
        this.MaterialId = MaterialId;
        this.CheckStockId = CheckStockId;
        this.remainingAmount = remainingAmount;
        this.expAmount = expAmount;
        this.totalLost = totalLost;
    }
    
    

    public CheckStockDetail(int MaterialId,int CheckStockId,double remainingAmount, int expAmount, int totalLost) {
        this.Id = nextId++;
        this.MaterialId = MaterialId;
        this.CheckStockId = CheckStockId;
        this.remainingAmount = remainingAmount;
        this.expAmount = expAmount;
        this.totalLost = totalLost;
    }

    public CheckStockDetail() {
        this.Id = -1;
        this.MaterialId = -1;
        this.CheckStockId = -1;
        this.remainingAmount = 0;
        this.expAmount = 0;
        this.totalLost = 0;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getMaterialId() {
        return MaterialId;
    }
    
    
    public void setMaterialId(int MaterialId) {
        this.MaterialId = MaterialId;
    }

    public int getCheckStockId() {
        return CheckStockId;
    }

    public void setCheckStockId(int CheckStockId) {
        this.CheckStockId = CheckStockId;
    }

    public double getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(double remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public int getExpAmount() {
        return expAmount;
    }

    public void setExpAmount(int expAmount) {
        this.expAmount = expAmount;
    }

    public int getTotalLost() {
        return totalLost;
    }

    public void setTotalLost(int totalLost) {
        this.totalLost = totalLost;
    }
    
//    public  void setMaterial(){
//        this.material = materialDao.get(MaterialId);
//    }
//    
//    public String getMaterialName(){
//        return material.getName();
//    }
//    
//    public double getOldAmount(){
//        return material.getQoh();
//    }
//    
//    public String getUnit(){
//        return material.getUnit();
//    }
    


    @Override
    public String toString() {
        return "CheckStockDetail{" + "Id=" + Id + ", MeterialId=" + MaterialId + ", CheckStockId=" + CheckStockId + ", remainningAmount=" + remainingAmount + ", expAmount=" + expAmount + ", totalLost=" + totalLost + '}';
    }

    public static CheckStockDetail fromRS (ResultSet rs) {
        CheckStockDetail checkStockDetail = new CheckStockDetail();
        try {
            checkStockDetail.setId(rs.getInt("SCD_CODE"));
            checkStockDetail.setMaterialId(rs.getInt("M_CODE"));
            checkStockDetail.setCheckStockId(rs.getInt("CHECK_CODE"));
            checkStockDetail.setRemainingAmount(rs.getDouble("SCD_REMAINING_AMOUNT"));
            checkStockDetail.setExpAmount(rs.getInt("SCD_EXPIRED_AMOUNT"));
            checkStockDetail.setTotalLost(rs.getInt("SCD_TOTAL_LOST"));
            
        } catch (SQLException ex) {
            Logger.getLogger(""+checkStockDetail.getId()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStockDetail;
    }

}
