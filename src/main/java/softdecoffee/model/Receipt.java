/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Lenovo
 */
public class Receipt {
//    private int id;
//    private Date createdDate;
//    private float total;
//    private float cash;
//    private int totalQty;
//    private int employeeId;
//    private int customerId;
//    private Employee employee;
//    private Customer customer;
//    private ArrayList<ReceiptDetail> receiptDetails = new ArrayList();
//
//    public Receipt(int id, Date createdDate, float total, float cash, int totalQty, int employeeId, int customerId) {
//        this.id = id;
//        this.createdDate = createdDate;
//        this.total = total;
//        this.cash = cash;
//        this.totalQty = totalQty;
//        this.employeeId = employeeId;
//        this.customerId = customerId;
//    }
//    
//    
//    public Receipt(Date createdDate, float total, float cash, int totalQty, int employeeId, int customerId) {
//        this.id = -1;
//        this.createdDate = createdDate;
//        this.total = total;
//        this.cash = cash;
//        this.totalQty = totalQty;
//        this.employeeId = employeeId;
//        this.customerId = customerId;
//    }
//    
//    public Receipt(float total, float cash, int totalQty, int employeeId, int customerId) {
//        this.id = -1;
//        this.createdDate = null;
//        this.total = total;
//        this.cash = cash;
//        this.totalQty = totalQty;
//        this.employeeId = employeeId;
//        this.customerId = customerId;
//    }
//    
//    public Receipt(float cash,int employeeId, int customerId) {
//        this.id = -1;
//        this.createdDate = null;
//        this.total = 0;
//        this.cash = cash;
//        this.totalQty = 0;
//        this.employeeId = employeeId;
//        this.customerId = customerId;
//    }
//    
//    
//    public Receipt() {        
//        this.createdDate = null;
//        this.total = 0;
//        this.cash = 0;
//        this.totalQty = 0;
//        this.employeeId = 0;
//        this.customerId =0;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public Date getCreatedDate() {
//        return createdDate;
//    }
//
//    public void setCreatedDate(Date createdDate) {
//        this.createdDate = createdDate;
//    }
//
//    public float getTotal() {
//        return total;
//    }
//
//    public void setTotal(float total) {
//        this.total = total;
//    }
//
//    public float getCash() {
//        return cash;
//    }
//
//    public void setCash(float cash) {
//        this.cash = cash;
//    }
//
//    public int getTotalQty() {
//        return totalQty;
//    }
//
//    public void setTotalQty(int totalQty) {
//        this.totalQty = totalQty;
//    }
//
//    public int getEmployeeId() {
//        return employeeId;
//    }
//
//    public void setEmployeeId(int employeeId) {
//        this.employeeId = employeeId;
//    }
//
//    public int getCustomerId() {
//        return customerId;
//    }
//
//    public void setCustomerId(int customerId) {
//        this.customerId = customerId;
//    }
//
//    public Employee getEmployee() {
//        return employee;
//    }
//
//    public void setEmployee(Employee employee) {
//        this.employee = employee;
//        this.employeeId = employee.getId();
//    }
//
//    public Customer getCustomer() {
//        return customer;
//    }
//
//    public void setCustomer(Customer customer) {
//        this.customer = customer;
//        this.customerId=customer.getId();
//    }
//
//    public ArrayList<ReceiptDetail> getReceiptDetails() {
//        return receiptDetails;
//    }
//
//    public void setReceiptDetails(ArrayList receiptDetails) {
//        this.receiptDetails = receiptDetails;
//    }
//
//    @Override
//    public String toString() {
//        return "Receipt{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", totalQty=" + totalQty + ", employeeId=" + employeeId + ", customerId=" + customerId + ", employee=" + employee + ", customer=" + customer + ", receiptDetails=" + receiptDetails + '}';
//    }
//    
//    public void addReceiptdetail(ReceiptDetail receiptDetail){
//        receiptDetails.add(receiptDetail);
//        calculateTotal();
//    }
//    
//    public void addReceiptdetail(Product product,int qty){
//        ReceiptDetail rd = new ReceiptDetail(product.getId(),product.getName(),product.getPrice(),qty,qty*product.getPrice(),-1);
//        receiptDetails.add(rd);
//        calculateTotal();
//    }
//    
//    public void delReceiptdetail(ReceiptDetail receiptDetail){
//        receiptDetails.remove(receiptDetail);
//        calculateTotal();
//    }
//    
//    public void calculateTotal(){
//        int totalQty = 0;
//        float total = 0.0f;
//        for(ReceiptDetail rd:receiptDetails){
//            total += rd.getTotalPrice();
//            totalQty += rd.getQty();           
//        }
//        this.totalQty = totalQty;
//        this.total = total;
//    }
//    
//    public static Receipt fromRS(ResultSet rs) {
//        Receipt receipt = new Receipt();
//        try {
//            receipt.setId(rs.getInt("receipt_id"));            
//            receipt.setCreatedDate(rs.getTimestamp("created_date"));
//            receipt.setTotal(rs.getFloat("total"));
//            receipt.setCash(rs.getFloat("cash"));
//            receipt.setTotalQty(rs.getInt("total_qty"));
//            receipt.setEmployeeId(rs.getInt("employee_id"));
//            receipt.setCustomerId(rs.getInt("customer_id"));
//            //Population
//            CustomerDao customerDao = new CustomerDao();
//            EmployeeDao employeeDao = new EmployeeDao();
//            Customer customer = customerDao.get(receipt.getCustomerId());
//            Employee employee = employeeDao.get(receipt.getEmployeeId());
//            receipt.setEmployee(employee);
//            receipt.setCustomer(customer);
//            
//            
//        } catch (SQLException ex) {
//            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
//            return null;
//        }
//        return receipt;
//    }
}
