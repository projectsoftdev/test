/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package softdecoffee.ui;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.table.AbstractTableModel;
import softdecoffee.dao.CheckStockDao;
import softdecoffee.dao.CheckStockDetailDao;
import softdecoffee.dao.MaterialDao;
import softdecoffee.model.CheckStock;
import softdecoffee.model.Material;
import softdecoffee.model.CheckStockDetail;
import softdecoffee.service.CheckStockService;
import softdecoffee.service.MaterialService;

/**
 *
 * @author Lenovo
 */
public class CheckStockDialog extends javax.swing.JDialog {
    //material-->check stock-->check stock detail
    ArrayList<Material> materials;
    MaterialService materialService = new MaterialService();
    CheckStockService checkStockService = new CheckStockService();
    CheckStock checkStock;
    private static CheckStockDialog instance;
    private CheckStock editedCheckStock;
    
    //private final ProductListPanel productListPanel;
    /**
     * Creates new form CheckStockDialog
     */
//    public CheckStockDialog(java.awt.Frame parent, CheckStock editedCheckStock) {
//        super(parent,true);
//        initComponents();
//        this.editedCheckStock =editedCheckStock;
//        CheckStockService checkStockService = new CheckStockService();
//        
//        
//    }
    
    /**
     * Creates new form PosPanel
     */
    public CheckStockDialog(java.awt.Frame parent, CheckStock editedCheckStock) {
        super(parent,true);
        initComponents();
        this.editedCheckStock = editedCheckStock;
        CheckStockService checkStockService = new CheckStockService();
      
        initMaterialTable();
        checkStock = new CheckStock();
        lblCheckStockId.setText(""+editedCheckStock.getId());
        //
        System.out.println("checkStockId = "+editedCheckStock.getId());
        //
        //checkStock.setUser(UserService.getCurrentUser());
        tblCheckStockDetail.setModel(new AbstractTableModel() {
            String[] headers = {"id", "Material Name", "Old Amount", "Remaining Amount", "Unit"};
            
            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                //ไว้ดูข้อมูล
                System.out.println("checkStock = "+checkStock);
                System.out.println("checkStock = "+checkStock.getCheckStockDetails());
                System.out.println("getRowCount = "+checkStock.getCheckStockDetails().size());
                //
                return checkStock.getCheckStockDetails().size();          
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<CheckStockDetail> checkStockDetails = checkStock.getCheckStockDetails();
                CheckStockDetail checkStockDetail = checkStockDetails.get(rowIndex);
                //get Material Name
                MaterialDao materialDao = new MaterialDao();
                int id = checkStockDetail.getMaterialId();
                Material material = materialDao.get(id);
                switch (columnIndex) {
                    case 0:
                        return checkStockDetail.getId();
                    case 1:
                        return material.getName();
                    case 2:
                        return material.getQoh();
                    case 3:
                        return checkStockDetail.getRemainingAmount();     
                    case 4:
                        return material.getUnit();    
                    default:
                        return "";
                }
            
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        });

        //productListPanel = new ProductListPanel();
        //productListPanel.addOnBuyProduct(this);
        //scrProductList.setViewportView(productListPanel);
    }

    private void initMaterialTable() {
        materials = materialService.getMaterialsOrderById();
        System.out.println("material = "+materials);
        tblMaterial.getTableHeader().setFont(new Font("TH Saraban New", Font.PLAIN, 16));
        tblMaterial.setRowHeight(100);
        tblMaterial.setModel(new AbstractTableModel() {
            String[] headers = {"Image", "ID", "Name", "Price"};

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                //
                System.out.println("getsize = "+materials.size());
                return materials.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material material = materials.get(rowIndex);
                //
                System.out.println("Materail as rowIndex : "+material);
                //
                switch (columnIndex) {
                    case 0:
                        ImageIcon icon = new ImageIcon("./Material" + material.getId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) ((float) (100 * width) / height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;
                    case 1:
                        return material.getId();
                    case 2:
                        return material.getName();
                    case 3:
                        return material.getQoh();
                    case 4:
                        return material.getPrice();                    
                    default:
                        return "";
                }

            }

        });
        tblMaterial.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblMaterial.rowAtPoint(e.getPoint());
                int col = tblMaterial.columnAtPoint(e.getPoint());
                //
                //System.out.println(materials.get(row));
                Material material = materials.get(row);
                
//                int id = Integer.parseInt(lblCheckStockId.getText());
//                System.out.println("getidfromtext = "+id);
                int checkStockId = checkStock.getId();
                double materialQoh = material.getQoh();
                checkStock.addCheckStockDetail(checkStockId,material,materialQoh, 1);
                refreshCheckStockDetail();

            }

        });
    }

    private void refreshCheckStockDetail() {
        tblCheckStockDetail.revalidate();
        tblCheckStockDetail.repaint();
        //lblTotal.setText("Total : " + checkStock.getTotal());
    }   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblCheckStockText = new javax.swing.JLabel();
        lblCheckStockId = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        scrMaterial = new javax.swing.JScrollPane();
        tblMaterial = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        scrCheckStockDetail = new javax.swing.JScrollPane();
        tblCheckStockDetail = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 204, 255));

        lblCheckStockText.setText("Check Stock ID : ");

        lblCheckStockId.setBackground(new java.awt.Color(204, 255, 204));
        lblCheckStockId.setOpaque(true);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCheckStockText)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCheckStockId, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblCheckStockText)
                        .addGap(0, 5, Short.MAX_VALUE))
                    .addComponent(lblCheckStockId, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        tblMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrMaterial.setViewportView(tblMaterial);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)
                .addContainerGap())
        );

        tblCheckStockDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrCheckStockDetail.setViewportView(tblCheckStockDetail);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStockDetail, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStockDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnSave.setText("SAVE");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setText("CLEAR");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnCancel.setText("CANCEL");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnClear)
                    .addComponent(btnCancel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        CheckStockDao checkStockDao = new CheckStockDao();
        CheckStockPanel checkStockPanel = new CheckStockPanel();
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        checkStockDao.save(checkStock);
        
        ArrayList<CheckStockDetail> checkStockDetails = checkStock.getCheckStockDetails();
        for(CheckStockDetail checkStockDetail:checkStockDetails){
            checkStockDetailDao.save(checkStockDetail);
        }
        checkStockPanel.refreshCheckStock();
        // หลังจากบันทึกข้อมูลเรียบร้อยแล้ว คุณสามารถปิดหน้าต่างปัจจุบันได้ด้วยคำสั่งนี้
        this.dispose();
        
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed
    
    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {                                        
       clearCheckStockDetail();
    }   
    
    private void clearCheckStockDetail() {
        checkStock.clearCheckStockDetails(); // เรียกเมธอดที่คุณจะต้องสร้างในคลาส CheckStock
        refreshCheckStockDetail(); // รีเฟรชตาราง tblCheckStockDetail
    }
    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSave;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lblCheckStockId;
    private javax.swing.JLabel lblCheckStockText;
    private javax.swing.JScrollPane scrCheckStockDetail;
    private javax.swing.JScrollPane scrMaterial;
    private javax.swing.JTable tblCheckStockDetail;
    private javax.swing.JTable tblMaterial;
    // End of variables declaration//GEN-END:variables
}
