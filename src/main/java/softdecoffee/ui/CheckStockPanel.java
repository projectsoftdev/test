/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package softdecoffee.ui;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import jdk.internal.foreign.PlatformLayouts;
import softdecoffee.dao.CheckStockDao;
import softdecoffee.dao.CheckStockDetailDao;
import softdecoffee.model.CheckStock;
import softdecoffee.model.CheckStockDetail;
import softdecoffee.service.CheckStockDetailService;
import softdecoffee.service.CheckStockService;

/**
 *
 * @author Lenovo
 */
public class CheckStockPanel extends javax.swing.JPanel {

    private ArrayList<CheckStock> checkStocks;
    private CheckStockService checkStockService = new CheckStockService();
    private CheckStockDetailService checkStockDetailService;
    CheckStock checkStock;
    CheckStock editedCheckStock;

    /**
     * Creates new form CheckStockPanel
     */
    public CheckStockPanel() {
        initComponents();
        initCheckStockTable();
        checkStock = new CheckStock();
        //lblUser.setText(UserService.getCurrentUser().getName());
        //checkStock.setEmployeeId(EmployeeService.getCurrentUser());
        tblCheckStockDetail.setModel(new AbstractTableModel() {
            String[] headers = {"id", "Material id", "CheckStock id", "Remaining Amount", "Expired Amount", "Total lost"};
            
            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                //ไว้ดูข้อมูล
                System.out.println("checkStock = "+checkStock);
                System.out.println("checkStock = "+checkStock.getCheckStockDetails());
                System.out.println("getRowCount = "+checkStock.getCheckStockDetails().size());
                //
                return checkStock.getCheckStockDetails().size();          
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<CheckStockDetail> checkStockDetails = checkStock.getCheckStockDetails();
                CheckStockDetail checkStockDetail = checkStockDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return checkStockDetail.getId();
                    case 1:
                        return checkStockDetail.getMaterialId();
                    case 2:
                        return checkStockDetail.getCheckStockId();
                    case 3:
                        return checkStockDetail.getRemainingAmount();
                    case 4:
                        return checkStockDetail.getExpAmount();
                    case 5:
                        return checkStockDetail.getTotalLost();
                    default:
                        return "";
                }
            }

//            @Override
//            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {          
//                    ArrayList<CheckStockDetail> checkStockDetails = checkStock.getCheckStockDetails(); 
//                    CheckStockDetail checkStockDetail = checkStockDetails.get(rowIndex);
//                    
//                    refreshCheckStockDetail();
//            }
//            @Override
//            public boolean isCellEditable(int rowIndex, int columnIndex) {
//                switch(columnIndex){
//                    case 2:
//                        return true;
//                    default:
//                        return false;
//                }
//            }
        });
    }

    private void initCheckStockTable() {
        checkStocks = checkStockService.getCheckStocksOrderById();
        tblCheckStock.getTableHeader().setFont(new Font("TH Saraban New", Font.PLAIN, 16));
        tblCheckStock.setRowHeight(100);
        tblCheckStock.setModel(new AbstractTableModel() {
            String[] headers = {"ID", "Employee ID", "DATETIME"};

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    //case 0:
                    //    return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return checkStocks.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckStock checkStock = checkStocks.get(rowIndex);
                switch (columnIndex) {
                    //case 0 :
                    //ImageIcon icon = new ImageIcon("./product" + checkStock.getId() + ".png");
                    //Image image = icon.getImage();
                    //int width = image.getWidth(null);
                    //int height = image.getHeight(null);
                    //Image newImage = image.getScaledInstance((int) ((float)(100*width)/height), 100, Image.SCALE_SMOOTH);
                    //icon.setImage(newImage);
                    //return icon; 
                    case 0:
                        return checkStock.getId();
                    case 1:
                        return checkStock.getEmployeeId();
                    case 2:
                        return checkStock.getCheckDate();
                    default:
                        return "";
                }

            }

        });
//        tblCheckStock.addMouseListener(new MouseAdapter() {
//        @Override
//        public void mouseClicked(MouseEvent e) {
//            int row = tblCheckStock.rowAtPoint(e.getPoint());
//            int col = tblCheckStock.columnAtPoint(e.getPoint());
//            System.out.println(checkStocks.get(row));
//            CheckStock checkStock = checkStocks.get(row);
//            int id = checkStock.getId();
//            //ไปgets แล้วมาใส่
//            CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
//            ArrayList<CheckStockDetail> checkStockDetails = checkStockDetailDao.getAllById(id);
//            checkStock.setCheckStockDetails(checkStockDetails);
////            CheckStockDetail checkStockDetail = checkStockDetailDao.get(id);
////            if (checkStockDetail != null) {
////                CheckStockDetail csd = new CheckStockDetail(checkStockDetail.getId(),checkStockDetail.getMaterialId(), checkStockDetail.getCheckStockId(), checkStockDetail.getRemainingAmount(), checkStockDetail.getExpAmount(), checkStockDetail.getTotalLost());
////                checkStock.addCheckStockDetail(csd);
////            }            
//            refreshCheckStockDetail();           
//        }
//    });
        
        tblCheckStock.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblCheckStock.rowAtPoint(e.getPoint());
                CheckStock editcheckStock = checkStocks.get(row);
                int id = editcheckStock.getId();

                CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
                ArrayList<CheckStockDetail> checkStockDetails = (ArrayList<CheckStockDetail>) checkStockDetailDao.getByCheckId(id);
                checkStock.setCheckStockDetails(checkStockDetails);
                
                //เอาไว้ดู
                System.out.println(id);
                System.out.println(checkStock.getCheckStockDetails());
                //
                refreshCheckStockDetail(); 
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        pnlHeader = new javax.swing.JPanel();
        lblUser = new javax.swing.JLabel();
        pnlCheckStock = new javax.swing.JPanel();
        scrCheckStock = new javax.swing.JScrollPane();
        tblCheckStock = new javax.swing.JTable();
        pnlCheckStockDetail = new javax.swing.JPanel();
        scrCheckStockDetail = new javax.swing.JScrollPane();
        tblCheckStockDetail = new javax.swing.JTable();
        pnlButton = new javax.swing.JPanel();
        btnMaterial = new javax.swing.JButton();
        btnAddCheckStock = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(255, 204, 204));

        pnlHeader.setBackground(new java.awt.Color(255, 255, 204));

        lblUser.setFont(new java.awt.Font("TH Sarabun New", 0, 18)); // NOI18N
        lblUser.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUser.setText("User :");

        javax.swing.GroupLayout pnlHeaderLayout = new javax.swing.GroupLayout(pnlHeader);
        pnlHeader.setLayout(pnlHeaderLayout);
        pnlHeaderLayout.setHorizontalGroup(
            pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlHeaderLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblUser, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlHeaderLayout.setVerticalGroup(
            pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblUser)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        pnlCheckStock.setBackground(new java.awt.Color(153, 255, 153));

        tblCheckStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrCheckStock.setViewportView(tblCheckStock);

        javax.swing.GroupLayout pnlCheckStockLayout = new javax.swing.GroupLayout(pnlCheckStock);
        pnlCheckStock.setLayout(pnlCheckStockLayout);
        pnlCheckStockLayout.setHorizontalGroup(
            pnlCheckStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCheckStockLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStock, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlCheckStockLayout.setVerticalGroup(
            pnlCheckStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCheckStockLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStock, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlCheckStockDetail.setBackground(new java.awt.Color(102, 255, 255));

        tblCheckStockDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrCheckStockDetail.setViewportView(tblCheckStockDetail);

        javax.swing.GroupLayout pnlCheckStockDetailLayout = new javax.swing.GroupLayout(pnlCheckStockDetail);
        pnlCheckStockDetail.setLayout(pnlCheckStockDetailLayout);
        pnlCheckStockDetailLayout.setHorizontalGroup(
            pnlCheckStockDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCheckStockDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStockDetail, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlCheckStockDetailLayout.setVerticalGroup(
            pnlCheckStockDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCheckStockDetailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrCheckStockDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlButton.setBackground(new java.awt.Color(255, 255, 255));

        btnMaterial.setText("Material");
        btnMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMaterialActionPerformed(evt);
            }
        });

        btnAddCheckStock.setText("ADD");
        btnAddCheckStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddCheckStockActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
        pnlButton.setLayout(pnlButtonLayout);
        pnlButtonLayout.setHorizontalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnMaterial)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAddCheckStock)
                .addContainerGap())
        );
        pnlButtonLayout.setVerticalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(btnAddCheckStock, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(pnlCheckStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlCheckStockDetail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlCheckStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlCheckStockDetail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMaterialActionPerformed
        openPanel();
    }//GEN-LAST:event_btnMaterialActionPerformed

    
    private void btnAddCheckStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddCheckStockActionPerformed
        editedCheckStock = new CheckStock();
        openDialog();
    }//GEN-LAST:event_btnAddCheckStockActionPerformed
    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CheckStockDialog userDialog = new CheckStockDialog(frame,editedCheckStock);
        userDialog.setLocationRelativeTo(this);
        userDialog.setVisible(true);
        userDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
            
            
        });
    }
    
    private void openPanel() {
        JFrame frame;
        MaterialJPanel materialJPanel = new MaterialJPanel();
        frame = (JFrame) SwingUtilities.getWindowAncestor(this);
        frame.setContentPane(materialJPanel);
        frame.revalidate(); // รีเฟรชหน้าต่าง
                   
    }
    
    private void refreshTable() {
        checkStocks = checkStockService.getCheckStocksArray();
        tblCheckStock.revalidate();
        tblCheckStock.repaint();
        tblCheckStockDetail.revalidate();
        tblCheckStockDetail.repaint();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddCheckStock;
    private javax.swing.JButton btnMaterial;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblUser;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlCheckStock;
    private javax.swing.JPanel pnlCheckStockDetail;
    private javax.swing.JPanel pnlHeader;
    private javax.swing.JScrollPane scrCheckStock;
    private javax.swing.JScrollPane scrCheckStockDetail;
    private javax.swing.JTable tblCheckStock;
    private javax.swing.JTable tblCheckStockDetail;
    // End of variables declaration//GEN-END:variables

    public void refreshCheckStock() {
        tblCheckStock.revalidate();
        tblCheckStock.repaint();
    }

    private void refreshCheckStockDetail() {
        tblCheckStockDetail.revalidate();
    }

}
